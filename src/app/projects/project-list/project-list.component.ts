import { Project } from '../../models/Project';
import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ngtt-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})
export class ProjectListComponent {

  @Input() projects: Project[];
  @Output() selected = new EventEmitter<Project>();
  @Output() over = new EventEmitter<Project>();

  selectProject(project: Project): void {
    this.selected.emit(project);
  }

  overProject(project: Project): void {
    this.over.emit(project);
  }

}
