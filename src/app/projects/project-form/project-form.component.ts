import { NgForm } from '@angular/forms';
import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'ngtt-project-form',
  templateUrl: './project-form.component.html',
  styleUrls: ['./project-form.component.css']
})
export class ProjectFormComponent {
  @Output() submitted = new EventEmitter();

  createNewProjectSubmit(form: NgForm): void {
    this.submitted.emit({ ...form.value });
  }
}
