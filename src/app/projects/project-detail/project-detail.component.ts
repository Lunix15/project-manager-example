import { ProjectService } from './../project.service';
import { Project } from './../../models/Project';
import { Component, Input } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'ngtt-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.css']
})
export class ProjectDetailComponent {
  @Input() project: Project;


  constructor(private activatedRoute: ActivatedRoute, private projectService: ProjectService) {
    const paramId: Params = this.activatedRoute.snapshot.params;
    this.project = this.projectService.getProject(+paramId.id);
  }
}
