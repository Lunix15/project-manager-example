import { ProjectService } from './../project.service';
import { Project } from '../../models/Project';
import { Component, Output, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';


@Component({
  selector: 'ngtt-project',
  templateUrl: './project-container.component.html',
  styleUrls: ['./project-container.component.css']
})
export class ProjectContainerComponent implements OnInit, OnDestroy {
  projects: Array<Project> = [
  ];
  subcription: Subscription[] = [];

  @Output() selectedProject: Project;
  @Output() searchedProject: Project;

  constructor(private projectService: ProjectService, private router: Router) {
  }

  ngOnDestroy(): void {
    for (const item of this.subcription) {
      item.unsubscribe();
    }
  }
  ngOnInit(): void {
    this.subcription.push(this.projectService.projects$.subscribe(initVal => this.projects = initVal));
  }

  selectProject(project: Project): void {
    this.selectedProject = project;
  }

  goToProject(id: number): void {
    this.router.navigate(['projects/detail/' + id]);
  }

  searchProject(request: Project): void {
    this.searchedProject = request;
  }

  addProject(project: Project): void {
    this.projectService.addProject(project);
  }

}
