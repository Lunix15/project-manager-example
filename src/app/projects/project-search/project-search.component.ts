import { Component, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { Project, Priority } from './../../models/Project';

@Component({
  selector: 'ngtt-project-search',
  templateUrl: './project-search.component.html',
  styleUrls: ['./project-search.component.css']
})
export class ProjectSearchComponent {
  @Output() searching = new EventEmitter<Partial<Project>>();

  @ViewChild('filterName') name: ElementRef;
  @ViewChild('filterPriority') priority: ElementRef;
  @ViewChild('filterDone') done: ElementRef;


  search(): void {
    const elName = (this.name.nativeElement as HTMLInputElement);
    const elPriority = (this.priority.nativeElement as HTMLInputElement);
    const elDone = (this.done.nativeElement as HTMLInputElement);

    // console.log('ricerca in corso...', this.name, this.priority, this.done);
    // console.log('ricerca in corso...', elName.value, elPriority.value, elDone.value);

    this.searching.emit({
      name: elName.value,
      priority: elPriority.value as Priority,
      done: !!elDone.value
    });
  }
}
