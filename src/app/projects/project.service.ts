import { Project } from './../models/Project';
import { Injectable } from '@angular/core';
import { interval, Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class ProjectService {
  private projects: Array<Project> = JSON.parse(localStorage.getItem('Projects')) || [{
    id: 1,
    code: 'NHusYJl',
    name: 'Progetto Alpha',
    description: 'Lorem ipsum dolor sit amet.',
    start: new Date(2019, 1, 31),
    end: new Date(2019, 3, 15),
    priority: 'medium',
    done: true,
    tasks: []
  },
  {
    id: 2,
    code: 'SJieYKl',
    name: 'Progetto Beta',
    description: 'Lorem ipsum dolor sit amet.',
    start: new Date(2019, 3, 31),
    end: new Date(2019, 6, 15),
    priority: 'low',
    done: true,
    tasks: []
  },
  {
    id: 3,
    code: 'POjeGBs',
    name: 'Progetto Gamma',
    description: 'Lorem ipsum dolor sit amet.',
    start: new Date(2019, 8, 15),
    priority: 'low',
    done: false,
    tasks: []
  },
  ];
  private projectObservable = new BehaviorSubject([...this.projects]);
  public projects$ = this.projectObservable.asObservable();


  // getProject(id: number): Observable<Project> {
  getProject(id: number): Project {
    return this.projects.find(project => project.id === id);
    // return this.projects$.pipe(
    //   map(projects: Project) => { return projects.find(project => project.id === id) }
    // )
  }

  getAll(): Observable<Project[]> {
    return this.projects$;
  }

  addProject(item: Project): void {
    this.projects.push({
      id: this.projects.length + 1,
      code: 'code_' + this.projects.length + 1,
      done: false,
      tasks: [],
      ...item
    });

    localStorage.setItem('Projects', JSON.stringify(this.projects));

    this.projectObservable.next([...this.projects]);
  }
}
