import { ProjectDetailComponent } from './projects/project-detail/project-detail.component';
import { ProjectContainerComponent } from './projects/project-container/project-container.component';
import { HomeComponent } from './home/home.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

export const routes: Routes = [
  { path: '', component: ProjectContainerComponent, pathMatch: 'full' },
  { path: 'projects', component: ProjectContainerComponent },
  { path: 'projects/detail/:id', component: ProjectDetailComponent },
  { path: 'home', component: HomeComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
