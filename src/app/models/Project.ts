import { Task } from './Task';

export type Priority = 'low' | 'high' | 'medium' | number;

export interface Project {
  id: number;
  code: string;
  name: string;
  description: string;
  company?: string;
  start: Date;
  end?: Date;
  priority?: Priority;
  done: boolean;
  tasks: Task[];
}
