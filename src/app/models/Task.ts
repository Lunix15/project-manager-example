export interface Task {
  id: number;
  name: string;
  start: Date;
  duration?: number;
  isBillable: boolean;
}

let task: Task = {
  id: 1,
  name: 'test',
  start: new Date('09/10/2020'),
  isBillable: false
}
