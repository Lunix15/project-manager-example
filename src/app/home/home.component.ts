import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngtt-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  title = 'gestoreProgetti';

  constructor() { }

  ngOnInit(): void {
  }

}
