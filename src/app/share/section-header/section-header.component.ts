import { Component, Input } from '@angular/core';

@Component({
  selector: 'ngtt-section-header',
  templateUrl: './section-header.component.html',
  styleUrls: ['./section-header.component.css']
})
export class SectionHeaderComponent {
  mbutton: Button = { label: 'etichetta del link', link: 'link alla risorsa', style: 'btn-link' };

  @Input() title = 'titolo della sezione';
  @Input() set button(value: Button) {
    this.mbutton = { ...this.mbutton, ...value };
  }
}

interface Button {
  label: string;
  link: string;
  style: 'btn-link' | 'btn-success';
}
