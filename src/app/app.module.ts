import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { SearchFilterPipe } from './common/search-filter.pipe';
import { AppRoutingModule } from './app-routing.module';

import { ProjectContainerComponent } from './projects/project-container/project-container.component';
import { ProjectDetailComponent } from './projects/project-detail/project-detail.component';
import { ProjectListComponent } from './projects/project-list/project-list.component';
import { ProjectFormComponent } from './projects/project-form/project-form.component';
import { ProjectSearchComponent } from './projects/project-search/project-search.component';

import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './layout/navbar/navbar.component';
import { SectionHeaderComponent } from './share/section-header/section-header.component';


// tslint:disable-next-line: jsdoc-format
/** RIAVVIARE L'APP ALLA MODIFICA DEL FILE**/

@NgModule({
  declarations: [
    AppComponent,
    ProjectContainerComponent,
    SearchFilterPipe,
    ProjectDetailComponent,
    ProjectListComponent,
    ProjectFormComponent,
    ProjectSearchComponent,
    HomeComponent,
    NavbarComponent,
    SectionHeaderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
